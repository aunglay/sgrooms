<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function up()
    {
       Schema::create('users', function(Blueprint $table){
          $table->increments('id');
          $table->string('name', 32);
          $table->string('email', 320);
          $table->timestamps('email_verified_at');
          $table->string('password', 64);
          $table->string('remember_token', 100)->nullable();
          $table->timestamps();
      });
    }
   

    public function down()
    {
        Schema::drop('users');
    }
}
